const express = require("express")
const app = express()
const bodyParse = require("body-parser")
const session = require('express-session')
const connection = require("./database/database")

const categoriesController = require("./categories/CategoriesController")
const articleController = require("./articles/ArticlesController")
const userControlller = require("./user/UserController") 

const Article = require("./articles/Article")
const Category = require("./categories/Category")
const User = require("./user/User")


//view engine
app.set('view engine', 'ejs')

//session
app.use(session({
    secret:"uchihaSoloLevingDeusGod", cookie:{maxAge: 1800000}
}))
//static
app.use(express.static('public'))

//bordy parse
app.use(bodyParse.urlencoded({extended:false}))
app.use(bodyParse.json())

//controllers
app.use("/", articleController)
app.use("/", categoriesController)
app.use("/", userControlller)

//database
connection
.authenticate()
.then(() => {
    console.log("Conexão com sucesso")
}).catch((error)=>{
    console.log("Error")
})

//Rotas

app.get("/session", (req, res)=>{
    req.session.treinamento = "Formaçaõ node js"
    req.session.email = "email"
    req.session.ano = 2021
    req.session.user = {
        email:"mayksuel@gmail.com",
        username: "mayk",
        id: 10
    }
    res.send("sessão gerada")
})

app.get("/leitura", (req, res)=>{
    res.json({
        treinamento: req.session.treinamento,
        ano: req.session.ano,
        email: req.session.email,
        user: req.session.user
    })
})

app.get("/", (req, res)=>{
    Article.findAll({
        order:[
            ['id','DESC']
        ],
        limit: 4
    }).then(articles =>{

        Category.findAll().then(categories =>{
            res.render("index", {articles:articles, categories:categories})
        })

    })
    
})

app.get("/:slug", (req, res)=>{
    var slug = req.params.slug
    Article.findOne({
        where:{
            slug: slug
        }
    }).then(article =>{
        if(article != undefined){
            Category.findAll().then(categories =>{
                res.render("article", {article:article, categories:categories})
            })
        }else{
            res.render("/")
        }
    }).catch(err =>{
        res.redirect("/")
    })
})

app.get("/category/:slug", (req, res)=>{
    var slug = req.params.slug
    Category.findOne({
        where: {
            slug: slug
        },
        include: [{model: Article}]
    }).then( category =>{
        if(category != undefined){
            Category.findAll().then(categories =>{
                res.render("index", {articles: category.articles, categories: categories})
            })
        }else{
            res.redirect("/")
        }
    }).catch(err=>{
        res.redirect("/")
    })
})




app.listen(2727, ()=>{
    console.log("O sevidor esta rodando!")
})